(ns drone-cl.simple
  (:require [clj-drone.core :refer :all]
            [clj-drone.navdata :refer :all]
            [clojure.string
             :as str]))




(drone-initialize)


 (+ 1 1)


(do

  (drone :flat-trim)
  (Thread/sleep 1000)

  (drone :take-off)
  (drone :hover)
  (drone :led_blink_snake)

  (Thread/sleep 2000)
  (drone :target-roundel-v)
  (drone :hover-on-roundel)

  (Thread/sleep 5000)
 ; (drone :land)
  )




(drone :emergency)

(drone-do-for 1 )



(drone :land)


(do
  (drone :flat-trim)
  (Thread/sleep 3000)
  (drone :take-off)
  (drone :hover)
  (Thread/sleep 1000)
  (drone :land)
  )




(drone :anim-yaw-shake)

(drone :anim-turnaround) ;=> kinda a jerky turnaround

(drone :anim-wave) ;=>  kinda like a hoola-hoop

(drone :anim-double-phi-theta-mixed) ;=> kinda crazy move had to describe

(drone :anim-flip-right) ;=> Classic flip goodness




(drone :led_blink_green_red) ;=> blink leds red green for 1 sec
(drone :led_blink_green) ;=> blink leds green for 1 sec
(drone :led_blink_red) ;=> blink leds red for 1 sec
(drone :led_blink_orange) ;=> blink leds orange for 1 sec



(drone-init-navdata)

@nav-data



(drone :nav-data)

(drone :emergency)

(drone :flat-trim)





(set-log-data [:seq-num :battery-percent :control-state :detect-camera-type
               :targets-num :targets])





(drone-initialize)

(drone :init-targeting)

(drone :target-shell-h)

(drone :target-color-blue)
  ;; the drone will look for targets with blue tags on the horizontal camera


;; the drone will look for the black and white roundel on the vertical camera

(drone-init-navdata)
  ;; watch the drone.log file and move the drone above the roundel
  ;; target and put the hull in front of the horizontal camera

(drone :take-off)




;;;



  ;; This puts the drone into a mode where it will hover on the roundel
  ;; and follow it around



(end-navstream)  ;; this ends the logging

R_x=[[1 0 0] [0 (cos alpha) (sin alpha)][ 0 (-sin alpha) (cos alpha)]]
R_y=[[(cos beta) 0 (-sin beta)] [0 1 0] [(sin beta) 0 (cos beta)]]
R_z=[[(cos gamma) (sin gamma) 0][(-sin gamma) (cos gamma) 0][0 0 1]]
